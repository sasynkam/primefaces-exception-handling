# PrimeFaces - Exception Handling

This is PrimeFaces app project to test non-ajax & ajax error handling.

## Documentation

* [OmniFaces FacesExceptionFilter](https://showcase.omnifaces.org/filters/FacesExceptionFilter)
* [OmniFaces FullAjaxExceptionHandler](https://showcase.omnifaces.org/exceptionhandlers/FullAjaxExceptionHandler)
* [Stackoverflow ViewExpiredException shown in java.lang.Throwable error-page in web.xml](https://stackoverflow.com/questions/3206922/viewexpiredexception-shown-in-java-lang-throwable-error-page-in-web-xml)

## Prerequisites - TomEE configuration

Project is using form authentication (see `web.xml`) to verify session time out. And also Single Sign On can be configured.

* Enable user definition in `TomEE/conf/tomcat-users.xhtml`

    ```xml
    <role rolename="tomcat"/>
    <role rolename="role1"/>
    <user username="tomcat" password="123" roles="tomcat"/>
    <user username="both" password="123" roles="tomcat,role1"/>
    <user username="role1" password="123" roles="role1"/>

    <role rolename="tomee-admin" />
    <user username="tomee" password="tomee" roles="tomee-admin,manager-gui" />
    ```

* Enable sharing of authentication (SSO) between applications in `TomEE/conf/server.xml`

    ```xml
    <Valve className="org.apache.catalina.authenticator.SingleSignOn" />
    ```

## Tips

* Login and wait until session expires (ViewExpiredException)
* Uncomment throwing of exceptions in `LoginController.actionJustTest()`
* Open app in multiple tabs/windows
* Deploy application twice (with different name) to verify SSO
