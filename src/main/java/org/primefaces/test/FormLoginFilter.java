package org.primefaces.test;

import org.apache.commons.lang3.StringUtils;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.security.InvalidParameterException;
import java.security.Principal;
import java.time.LocalDateTime;

public class FormLoginFilter implements Filter {

    private static final String LOGIN_PAGE_FILTER_PARAM = "LOGIN_PAGE";

    private String loginPage;

    @Override
    public void init(FilterConfig filterConfig) {
        final String loginPageParam = filterConfig.getInitParameter(LOGIN_PAGE_FILTER_PARAM);
        if (StringUtils.isBlank(loginPageParam)) {
            throw new InvalidParameterException("Debug: Login page not specified!");
        } else {
            loginPage = loginPageParam;
        }
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {

        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        HttpSession session = request.getSession(false);
        Principal principal = request.getUserPrincipal();

        if (session == null || principal == null) {
            System.out.println(LocalDateTime.now() + " Debug: LoginFilter.doFilter() session [" + session + "] or principal [" + principal + "] null, redirecting to login page");
            response.sendRedirect(request.getContextPath() + "/" + loginPage);
        } else {
            System.out.println(LocalDateTime.now() + " Debug: LoginFilter.doFilter() continue chain");
            filterChain.doFilter(servletRequest, servletResponse);
        }
    }

    @Override
    public void destroy() {

    }
}
