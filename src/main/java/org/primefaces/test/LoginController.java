package org.primefaces.test;

import javax.faces.application.ViewExpiredException;
import javax.faces.context.ExternalContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.Serializable;
import java.time.LocalDateTime;

@Named
@ViewScoped
public class LoginController implements Serializable {

    @Inject
    private ExternalContext externalContext;

    private String username;
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void actionLogin() throws IOException {
        HttpServletRequest request = (HttpServletRequest) externalContext.getRequest();

        try {
            request.login(username, password);
            // externalContext.getSessionMap().put("user", user);
            externalContext.redirect(externalContext.getRequestContextPath() + "/xhtml/restricted/restricted.xhtml");
            System.out.println(LocalDateTime.now() + " Debug: LoginController.actionLogin() Login Success");

        } catch (ServletException e) {
            // Display user-friendly error message to the user
            System.out.println(LocalDateTime.now() + " Debug: LoginController.actionLogin() Login Failed: " + e.getMessage());
        }
    }

    public void actionJustTest() {
        System.out.println(LocalDateTime.now() + " Debug: LoginController.actionJustTest()");
        // throw new ViewExpiredException("Something went wrong");
        // throw new RuntimeException("Something went wrong");
    }

    public void actionLogout() throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) externalContext.getRequest();
        request.logout();
        externalContext.invalidateSession();
        externalContext.redirect(externalContext.getRequestContextPath());
    }

    public String getPrincipal() {
        return externalContext.getUserPrincipal() != null ? externalContext.getUserPrincipal().getName() : "null";
    }

    public int getSessionMaxInactiveInterval() {
        return externalContext.getSessionMaxInactiveInterval();
    }
}
