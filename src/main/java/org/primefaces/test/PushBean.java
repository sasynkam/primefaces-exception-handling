package org.primefaces.test;

import javax.enterprise.context.ApplicationScoped;
import javax.faces.push.Push;
import javax.faces.push.PushContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Calendar;

@Named
@ApplicationScoped
public class PushBean implements Serializable {

    @Inject
    @Push(channel = "clock")
    private PushContext push;

    public void clockAction() {
        Calendar now = Calendar.getInstance();

        final String time = now.get(Calendar.HOUR_OF_DAY) + ":" + now.get(Calendar.MINUTE) + ":" + now.get(Calendar.SECOND);

        System.out.println(LocalDateTime.now() + " Debug: PushBean.clockAction() Time:" + time);

        push.send(time);
    }
}
