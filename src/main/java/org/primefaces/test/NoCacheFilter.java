package org.primefaces.test;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static javax.faces.application.ResourceHandler.RESOURCE_IDENTIFIER;

public class NoCacheFilter implements Filter {

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        String resourcePath = request.getContextPath() + RESOURCE_IDENTIFIER;

        if (!request.getRequestURI().startsWith(resourcePath)) {
            // https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Cache-Control
            response.setHeader("Cache-Control", "no-store"); // HTTP 1.1
            // https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Pragma
            response.setHeader("Pragma", "no-cache"); // HTTP 1.0
            // https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Expires
            response.setDateHeader("Expires", 0); // prevents caching at the proxy server
        }
        filterChain.doFilter(request, response);
    }
}
