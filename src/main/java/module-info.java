module primefaces.app.web {
    // internal
    // external
    requires javax.inject;
    requires cdi.api;
    requires myfaces.api;
    requires primefaces;
    requires java.annotation;
    requires tomcat.servlet.api;
    requires org.apache.commons.lang3;
}